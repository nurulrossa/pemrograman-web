<!DOCTYPE HTML>  
<html>
<head>
  <title>Tugas Validasi Data</title>
<style>
.kosong {color: #FF0000;}
</style>
</head>
<body>  

<?php
// define variables and set to empty values
$namaErr = $nimErr = "";
$nama = $nim = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["nama"])) {
    $namaErr = "Nama tidak boleh kosong";
  } else {
    $nama = test_input($_POST["nama"]);
 }
  if (empty($_POST["nim"])) {
    $nimErr = "NIM tidak boleh kosong";
  } else {
    $nim = test_input($_POST["nim"]);
    if (!preg_match("/^[0-9]*$/",$nim)) {
      $nimErr = "harus berupa angka dan panjangnya 10 karakter"; 
  }
  } 

}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>

<h2>VALIDASI DATA</h2>
<p> <span class="kosong">* Wajib Diisi</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
  Nama:<input type="text" name="nama">
  <span class="kosong">* <?php echo $namaErr;?></span>
  <br><br>
  NIM:<input type="text" name="nim">
  <span class="kosong">* <?php echo $nimErr;?></span>
  <br><br> 
  <input type="submit" name="submit" value="Submit">  
</form>

<?php
echo "<h2>Selamat Datang</h2>";
echo $nama;
echo "<br>";
echo $nim;
?>

</body>
</html>